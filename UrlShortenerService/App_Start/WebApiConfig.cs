﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using UrlShortenerService.Handler;

namespace UrlShortenerService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            HandlerFunction tokenInspector = new HandlerFunction() { InnerHandler = new HttpControllerDispatcher(config) };
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

                config.Routes.MapHttpRoute(
                name: "UrlMapping",
                routeTemplate: "{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: null,
                handler: tokenInspector
            );
        }
    }
}
