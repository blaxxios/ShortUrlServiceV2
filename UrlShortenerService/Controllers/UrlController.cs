﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UrlShortenerService.Controllers
{
    public class UrlController : Controller
    {
        // GET: Url

        [HttpGet,ActionName("getshorturl")]
        public ActionResult GetShortUrl()
        {
            try
            {

                if (Request.ContentType != null)
                {
                    var query = Request.Url.Query.Replace("?", "");
                    var querySplit = query.Split('&').ToList();
                    var findLongUrl = querySplit.Where(x => x.ToUpper().Contains("LONGURL")).FirstOrDefault().Split('=')[1];
                    findLongUrl = (findLongUrl.Contains("http://") || findLongUrl.Contains("https://")) ? findLongUrl : "http://" + findLongUrl;
                    dynamic data = new ExpandoObject();
                    data.ShortUrl = ShortUrlParse(new Uri(findLongUrl));
                    var jsonResult = JsonConvert.SerializeObject(data);

                    ViewBag.DataUrl = jsonResult;

                    //response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
                    //response.StatusCode = HttpStatusCode.OK;
                }
                else {
                    TempData["MessagePage"] = "Request Failed";
                    return RedirectToAction("page");
                }
            }
            catch (Exception ex)
            {
                TempData["MessagePage"] = "Error" + ex.Message;
                return RedirectToAction("page");
            }
            
            return View();
        }


        [HttpGet, ActionName("mapping")]
        public ActionResult Mapping()
        {
            var query = Request.Url.Query.Replace("?", "");

            var urlShortenerModel = new Models.UrlShortener().GetShortenerUrl(query.Split('=')[1]);
            //var response = Request.CreateResponse(HttpStatusCode.Moved);
            if (DateTime.Now > urlShortenerModel.ExpiredDate)
            {
                TempData["MessagePage"] = "Not Found";
                return RedirectToAction("page");
            }
            else {
                return Redirect(urlShortenerModel.LongUrl);
            }

            return View();
        }


        [HttpGet, ActionName("page")]
        public ActionResult Page()
        {
            ViewBag.Data = TempData["MessagePage"];
            return View();
        }

        #region Function
        private Random random = new Random();
        string ShortUrlParse(Uri longUrl)
        {
            var result = "";
            try
            {
                var baseUrl = longUrl.AbsoluteUri.Remove(longUrl.AbsoluteUri.Length - longUrl.AbsolutePath.Length, longUrl.AbsolutePath.Length);
                //var urlParse= baseUrl + "/" + RandomString(6);
                var shortUrl = ConfigurationManager.AppSettings["baseUrlShort"] + "/" + RandomString(6);
                if (new Models.UrlShortener().Save(new Models.UrlShortener()
                {
                    LongUrl = longUrl.ToString(),
                    ShortUrl = shortUrl,
                    CreateDate = DateTime.Now.ToLocalTime(),
                    ExpiredDate = DateTime.Now.AddDays(7).ToLocalTime()

                }))
                {
                    result = shortUrl;
                }

            }
            catch (Exception ex)
            {


            }
            return result;
        }
        private string RandomString(int length)
        {
            const string chars = "qwertyuiopasdfghjklzxcvbnmABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        #endregion
    }
}