﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace UrlShortenerService.Models
{
    public class UrlShortener
    {
        public int Id { get; set; }
        public string ShortUrl { get; set; }
        public string LongUrl { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpiredDate { get; set; }

        public Models.UrlShortener GetShortenerUrl(string shortUrl)
        {
            var urlModel = new Models.UrlShortener();
            try
            {
                var connString = ConfigurationManager.ConnectionStrings["ConnString"];
                using (var connection = new SqlConnection(connString.ConnectionString))
                {
                    connection.Open();
                    using (var command = new SqlCommand("Select * from shortener_url where short_url='" + shortUrl + "'", connection))
                    {
                        command.CommandType = CommandType.Text;
                        var datareader = command.ExecuteReader();
                        while (datareader.Read())
                        {
                            urlModel = new UrlShortener()
                            {
                                Id = Convert.ToInt32(datareader[0]),
                                ShortUrl = datareader[1].ToString(),
                                LongUrl = datareader[2].ToString(),
                                CreateDate = Convert.ToDateTime(datareader[3]),
                                ExpiredDate = Convert.ToDateTime(datareader[4])
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return urlModel;
        }
        public bool Save(Models.UrlShortener data)
        {
            var result = false;
            try
            {
            Run:
                var connString = ConfigurationManager.ConnectionStrings["ConnString"];
                using (var connection = new SqlConnection(connString.ConnectionString))
                {
                    connection.Open();
                    using (var command = new SqlCommand("Insert into shortener_url(short_url,long_url,create_date,expired_date) values('" + data.ShortUrl + "','" + data.LongUrl + "','" + data.CreateDate + "','" + data.ExpiredDate + "')", connection))
                    {
                        command.CommandType = CommandType.Text;
                        result = command.ExecuteNonQuery() == 1;
                    }
                }
                if (!result) { goto Run; }
            }
            catch (Exception ex)
            {
            }
            return result;
        }
    }
}