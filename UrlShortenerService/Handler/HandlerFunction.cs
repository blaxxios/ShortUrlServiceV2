﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace UrlShortenerService.Handler
{
    public class HandlerFunction :DelegatingHandler
    {
       
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage reply = request.CreateResponse(HttpStatusCode.Moved);
            if (request.RequestUri.Segments.Count() == 2)
            {             
                var requestUri = request.RequestUri;
                var baseUrl = request.RequestUri.AbsoluteUri.Remove(request.RequestUri.AbsoluteUri.Length - request.RequestUri.AbsolutePath.Length, request.RequestUri.AbsolutePath.Length);
                reply.Headers.Location = new Uri(ConfigurationManager.AppSettings["baseUrlShort"] + "/url/mapping?uri=" + request.RequestUri.AbsoluteUri);                
            }
            else
            {
                reply.Headers.Location = new Uri(ConfigurationManager.AppSettings["baseUrlShort"] + "/url/mapping?uri=" + request.RequestUri.AbsoluteUri);
            }
           
            return Task.FromResult(reply);

        }

     
    }
}